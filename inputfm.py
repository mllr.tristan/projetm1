import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QWidget, QMainWindow, QPushButton, QAction, QLineEdit, QMessageBox
import socket

class MainWindow(QWidget):

    def __init__(self):
        super(MainWindow, self).__init__()
        
        self.layout = QVBoxLayout()

        self.setWindowTitle("My Own Title")

        #IP :
        self.labelip = QLabel("IP Address of the RPi :")
        self.layout.addWidget(self.labelip)
        self.ipbox = QLineEdit(self)
        self.ipbox.setText("192.168.137.")
        self.layout.addWidget(self.ipbox)

        # Freq : 
        self.labelfreq = QLabel("Frequency in MHz :")
        self.layout.addWidget(self.labelfreq)
        self.fbox = QLineEdit(self)
        self.layout.addWidget(self.fbox)
        
        # Create a button in the window
        self.button = QPushButton('Send', self)
        self.layout.addWidget(self.button)
        
        # connect button to function on_click
        self.button.clicked.connect(self.on_click)

        
        self.setLayout(self.layout)
        self.show()
    
    @pyqtSlot()
    def on_click(self):
        ipValue = self.ipbox.text()
        fValue = str(int(float(self.fbox.text())*10))
        #QMessageBox.question(self, 'Titre', "You typed: " + textboxValue, QMessageBox.Ok, QMessageBox.Ok)
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((ipValue, 5555))
        print('Connexion vers ' + ipValue + ':' + str(5555) + ' reussie.')
        print('Envoi de :' + fValue)
        n = client.send(fValue)
        if (n != len(fValue)):
            print('Erreur envoi.')
        else:
            print('Envoi ok.')
        print('Reception...')
        donnees = client.recv(1024)
        print('Recu :', donnees)
        print('Deconnexion.')
        client.close()



if __name__ == "__main__":
    app = QApplication(sys.argv)
    mw = MainWindow()
    mw.show()
    sys.exit(app.exec_())




