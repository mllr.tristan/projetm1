# this module will be imported in the into your flowgraph

import socket

def server(tb):
    
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the address given on the command line
    server_name = "192.168.2.2"
    server_address = (server_name, 5556)
    print('starting up on %s port %s' % server_address)
    sock.bind(server_address)
    sock.listen(1)

    while True:
        print('waiting for a connection')
        connection, client_address = sock.accept()
        try:
            print('client connected:', client_address)
            while True:
                data = connection.recv(16)
                print('received "%s"' % data)
                if data:
                    connection.sendall(data)
                    tb.set_freq(float(data)/10)
                    break
                else:
                    break
        finally:
            connection.close()
