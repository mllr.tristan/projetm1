# this module will be imported in the into your flowgraph

import socket
import subprocess, threading
import os,signal
import time

# CLASS FROM : https://stackoverflow.com/questions/27588919/how-to-call-a-system-command-with-specified-time-limit-in-python
class Command(object):
    def __init__(self, cmd):
        self.cmd = cmd
        self.process = None

    def run(self, timeout):
        def target():
            print('Thread started')
            self.process = subprocess.Popen(self.cmd, shell=True, preexec_fn=os.setsid)
            self.process.communicate()
            print('Thread finished')

        thread = threading.Thread(target=target)
        thread.start()

        thread.join(timeout)
        if thread.is_alive():
            print('Terminating process')
            os.killpg(self.process.pid, signal.SIGTERM)
            thread.join()
        print(self.process.returncode)

def server(tb):
    
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the address given on the command line
    server_name = "192.168.2.2"
    server_address = (server_name, 5556)
    print('starting up on %s port %s' % server_address)
    sock.bind(server_address)
    sock.listen(1)

    while True:
        print('waiting for a connection')
        connection, client_address = sock.accept()
        try:
            print('client connected:', client_address)
            while True:
                data = connection.recv(32)
                print('received "%s"' % data)
                if data:
                    connection.sendall(data)
                    #cmd = "./pi_fm_rds -freq {:.2f}".format(float(data)/500)
                    cmd = data.decode("utf-8")
                    print("CMD: "+cmd)
                    command = Command(cmd)
                    t = cmd.split() #Split the command to be able to get the duration for the timeout
                    command.run(timeout=int(float(t[-1]))+5)
                    time.sleep(2)
                    break
                else:
                    break
        finally:
            connection.close()
