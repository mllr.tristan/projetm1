#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Not titled yet
# Author: tristan
# GNU Radio version: 3.8.1.0

from distutils.version import StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

from PyQt5 import Qt
from gnuradio import audio
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import zeromq
from gnuradio.qtgui import Range, RangeWidget
from gnuradio import qtgui
import socket

class webfmpc(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Not titled yet")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Not titled yet")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "webfmpc")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Variables
        ##################################################
        self.slide = slide = 96.9
        self.samp_rate = samp_rate = 48000
        self.Send = Send = 0

        ##################################################
        # Blocks
        ##################################################
        self.zeromq_sub_source_0 = zeromq.sub_source(gr.sizeof_float, 1, 'tcp://192.168.2.2:5555', 100, False, -1)
        self._slide_range = Range(96, 104, 0.1, 96.9, 200)
        self._slide_win = RangeWidget(self._slide_range, self.set_slide, 'FM Frequency', "counter_slider", float)
        self.top_grid_layout.addWidget(self._slide_win)
        self.audio_sink_0 = audio.sink(samp_rate, '', True)
        _Send_push_button = Qt.QPushButton('Send')
        _Send_push_button = Qt.QPushButton('Send')
        self._Send_choices = {'Pressed': 1, 'Released': 0}
        _Send_push_button.pressed.connect(lambda: self.set_Send(self._Send_choices['Pressed']))
        _Send_push_button.released.connect(lambda: self.set_Send(self._Send_choices['Released']))
        self.top_grid_layout.addWidget(_Send_push_button)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.zeromq_sub_source_0, 0), (self.audio_sink_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "webfmpc")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_slide(self):
        return self.slide

    def set_slide(self, slide):
        #print(slide)
        ipValue = "192.168.2.2"
        fValue = bytes(str(int(slide*10)),"utf-8")
        #QMessageBox.question(self, 'Titre', "You typed: " + textboxValue, QMessageBox.Ok, QMessageBox.Ok)
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((ipValue, 5556))
        print('Connexion vers ' + ipValue + ':' + str(5556) + ' reussie.')
        print('Envoi de :')
        print(fValue)
        n = client.send(fValue)
        if (n != len(fValue)):
            print('Erreur envoi.')
        else:
            print('Envoi ok.')
        print('Reception...')
        donnees = client.recv(1024)
        print('Recu :', donnees)
        print('Deconnexion.')
        client.close()
        self.slide = slide

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

    def get_Send(self):
        return self.Send

    def set_Send(self, Send):
        self.Send = Send



def main(top_block_cls=webfmpc, options=None):

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def sig_handler(sig=None, frame=None):
        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    def quitting():
        tb.stop()
        tb.wait()
    qapp.aboutToQuit.connect(quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
